package ru.mail.agb88.service;

import ru.mail.agb88.model.Role;
import ru.mail.agb88.model.User;
import ru.mail.agb88.repository.UserRepository;

/**
 * Методы для пользователей
 */
public class UserService {

    private static UserService instance;

    private UserService() {
    }

    public static UserService getInstance() {
        if (instance == null) {
            instance = new UserService();
        }
        return instance;
    }


    public boolean checkAdmin(User user) {
        return user.getRole() == Role.ADMINISTRATOR;
    }

    // Получает конкретного пользователя из БД по логину и паролю
    public User getUserByLoginAndPassword(String login, String password) {
        return UserRepository.getInstance().getUserByLoginAndPassword(login, password);
    }

    // Получает конкретного пользователя из БД по id
    public User getUserById(Integer userId) {
        return UserRepository.getInstance().getUserById(userId);
    }

    public boolean checkLoginAndPassword(String login, String password) {
        return (login != null && !login.equals("") && password != null && !password.equals(""));
    }
}
