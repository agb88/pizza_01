package ru.mail.agb88.service;

import org.apache.log4j.Logger;
import ru.mail.agb88.model.Order;
import ru.mail.agb88.model.Pizza;
import ru.mail.agb88.model.User;
import ru.mail.agb88.repository.PizzaRepository;

import java.util.List;

/**
 * Все, что связано с пиццей
 */
public class PizzaService {

    private static final Logger logger = Logger.getLogger(PizzaService.class);
    private static PizzaService Instance;

    public static PizzaService getInstance() {
        if (Instance == null) {
            Instance = new PizzaService();
        }
        return Instance;
    }

    // Возвращает список всех пицц из меню
    public List<Pizza> getAllPizzas() {
        return PizzaRepository.getInstance().getAllPizzas();
    }

    // Возвращает список заказов от ВСЕХ пользователей
    public List<Order> getAllOrdersFromUsers() {
        return PizzaRepository.getInstance().getAllOrdersFromUsers();
    }

    // Возвращает список всех пицц заказанных конкретным пользователем
    public List<Order> getAllPizzasForCurrentUser(User user) {
        return PizzaRepository.getInstance().getAllPizzasForCurrentUser(user);
    }

    // Меняет статус пиццы
    public void changeOrderStatus(Integer userId, Integer pizzaId, String status) {
        PizzaRepository.getInstance().changeOrderStatus(userId, pizzaId, status);
    }

    // Добавляет пиццу конкретного пользователя в заказ
    public void addPizza(Integer pizzaId, User user) {
        Integer pizzaQuantity = PizzaRepository.getInstance().getQuantityThisPizza(pizzaId, user); // получаем количество пицц в заказе

        if (logger.isInfoEnabled()) {
            logger.info("Current pizzas quantity = " + pizzaQuantity);
        }

        if (pizzaQuantity > 0) {
            pizzaQuantity++;
            PizzaRepository.getInstance().command(pizzaId, pizzaQuantity, user, "addPizza");
        } else {
            PizzaRepository.getInstance().command(pizzaId, pizzaQuantity, user, "addFirstPizza");
        }
    }

    // Удаляет 1 пиццу из заказа конкретного пользователя
    public void removeOnePizza(Integer pizzaId, User user) {
        Integer pizzaQuantity = PizzaRepository.getInstance().getQuantityThisPizza(pizzaId, user);

        if (logger.isInfoEnabled()) {
            logger.info("Current pizzas quantity = " + pizzaQuantity);
        }

        if (pizzaQuantity != 1) {
            pizzaQuantity--;
            PizzaRepository.getInstance().command(pizzaId, pizzaQuantity, user, "removeOnePizza");
        } else {
            PizzaRepository.getInstance().command(pizzaId, pizzaQuantity, user, "deletePizza");
        }
    }

    // Удаляет все пиццы одного наименования из заказа конкретного пользователя
    public void deletePizza(Integer pizzaId, User user) {
        PizzaRepository.getInstance().command(pizzaId, null, user, "deletePizza");
    }
}
