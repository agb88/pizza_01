package ru.mail.agb88.repository;

import org.apache.log4j.Logger;
import ru.mail.agb88.model.Role;
import ru.mail.agb88.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Методы связи пользователя с БД
 */
public class UserRepository {

    private static final Logger logger = Logger.getLogger(UserRepository.class);
    private static UserRepository instance;

    public static UserRepository getInstance() {
        if (instance == null) {
            instance = new UserRepository();
        }
        return instance;
    }

    // Получает конкретного пользователя из БД по логину и паролю
    public User getUserByLoginAndPassword(String login, String password) {
        User user = null;

        if (logger.isInfoEnabled()) {
            logger.info("Getting user by login and password");
        }

        try (Connection cn = ConnectionConfig.getInstance().getConnection();  // соединение
             PreparedStatement pt = cn.prepareStatement("SELECT * FROM users WHERE login = ? AND password = ?")) { // стейтмент
            pt.setString(1, login);
            pt.setString(2, password);
            ResultSet rs = pt.executeQuery(); // результат запроса
            while (rs.next()) {
                Role role = Role.valueOf(rs.getString(4));
                user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), role);

                if (logger.isInfoEnabled()) {
                    logger.info("Got user = " + user.getLogin() + " by login and password");
                }
                break;
            }
        } catch (SQLException e) {
            logger.error("Error of getting user by login and password", e);
        }
        return user;
    }

    // Получает конкретного пользователя из БД по id
    public User getUserById(Integer userId) {
        User user = null;

        if (logger.isInfoEnabled()) {
            logger.info("Getting user by id");
        }

        try (Connection cn = ConnectionConfig.getInstance().getConnection();  // соединение
             PreparedStatement pt = cn.prepareStatement("SELECT * FROM users WHERE id = ?")) {
            pt.setInt(1, userId);
            ResultSet rs = pt.executeQuery();  // результат запроса
            while (rs.next()) {
                Role role = Role.valueOf(rs.getString(4));
                user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), role);

                if (logger.isInfoEnabled()) {
                    logger.info("Got user = " + user.getLogin() + " by id");
                }
                break;
            }
        } catch (SQLException e) {
            logger.error("Error of getting user by id", e);
        }
        return user;
    }
}
