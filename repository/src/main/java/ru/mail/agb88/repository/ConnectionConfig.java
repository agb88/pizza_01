package ru.mail.agb88.repository;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;


/**
 * Конфигурация соединения
 */
public class ConnectionConfig {

    private static final Logger logger = Logger.getLogger(ConnectionConfig.class);
    private static ConnectionConfig instance;

    public static ConnectionConfig getInstance() {
        if (instance == null) {
            instance = new ConnectionConfig();
        }
        return instance;
    }

    // настройки соединения с БД
    private String getUrl() {
        return getResourceBundle("database").getString("url");
    }

    // настройки соединения с БД
    private Properties getProps() {
        ResourceBundle rb = getResourceBundle("database");
        Properties props = new Properties();
        props.put("user", rb.getString("user"));
        props.put("password", rb.getString("password"));
        props.put("autoReconnect", "true");
        props.put("characterEncoding", "UTF-8");
        props.put("useUnicode", "true");
        return props;
    }

    private ResourceBundle getResourceBundle(String database) {
        return ResourceBundle.getBundle(database);
    }

    public Connection getConnection() {
        Connection cn = null;
        try {
            cn = DriverManager.getConnection(ConnectionConfig.getInstance().getUrl(), ConnectionConfig.getInstance().getProps());
        } catch (SQLException e) {
            logger.error("Error to create connection in ConnectionConfig", e);
        }
        return cn;
    }
}
