package ru.mail.agb88.repository;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/**
 * Работа с БД без участия пользователей и товара
 */
public class DataBaseRepository {

    private static final Logger logger = Logger.getLogger(DataBaseRepository.class);
    private static DataBaseRepository instance;

    private DataBaseRepository() {
    }

    public static DataBaseRepository getInstance() {
        if (instance == null) {
            instance = new DataBaseRepository();
        }
        return instance;
    }

    // Инициализирует базу данных таблицами
    public void initializeDB() {
        try {
            Class.forName("com.mysql.jdbc.Driver"); // регистрируем долбаный драйвер
        } catch (ClassNotFoundException e) {
            logger.error("Do not register driver", e);
        }
        try (Scanner scanner = new Scanner(new File(getClass().getClassLoader().getResource("prop.sql").getFile()), "UTF-8");
             Connection cn = ConnectionConfig.getInstance().getConnection();
             Statement st = cn.createStatement()) {
            if (logger.isInfoEnabled()) {
                logger.info("Connection done");
            }
            while (scanner.hasNext()) {
                String query = scanner.nextLine();
                if (logger.isInfoEnabled()) {
                    logger.info("Setting query: " + query);
                }
                st.executeUpdate(query);
            }
        } catch (SQLException e) {
            logger.error("Error to create connection", e);
        } catch (FileNotFoundException e) {
            logger.error("Can't read prop.sql", e);
        }
    }
}
