package ru.mail.agb88.repository;

import org.apache.log4j.Logger;
import ru.mail.agb88.model.Order;
import ru.mail.agb88.model.Pizza;
import ru.mail.agb88.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Все что связано с пиццей
 */
public class PizzaRepository {

    private static final Logger logger = Logger.getLogger(PizzaRepository.class);
    private static PizzaRepository instance;

    public static PizzaRepository getInstance() {
        if (instance == null) {
            instance = new PizzaRepository();
        }
        return instance;
    }

    // Возвращает список всех пицц из БД
    public List<Pizza> getAllPizzas() {
        List<Pizza> pizzas = new ArrayList<>();

        if (logger.isInfoEnabled()) {
            logger.info("Getting list of all pizzas from DB");
        }

        try (Connection cn = ConnectionConfig.getInstance().getConnection();  // соединение
             Statement st = cn.createStatement(); // стейтмент
             ResultSet rs = st.executeQuery("SELECT * FROM mydb.pizzas")) { // результат запроса
            while (rs.next()) {
                pizzas.add(new Pizza(rs.getInt(1), rs.getString(2)));
            }
        } catch (SQLException e) {
            logger.error("Error of getting AllPizzas", e);
        }

        if (logger.isInfoEnabled()) {
            logger.info("Got list of all pizzas from DB");
        }
        return pizzas;
    }

    // Возвращает список заказов от ВСЕХ пользователей
    public List<Order> getAllOrdersFromUsers() {
        List<Order> allOrders = new ArrayList<>();
        String query = "SELECT orders.id, users.id, pizzas.id, pizzas.name, orders.quantity, orders.status FROM Orders INNER JOIN Users ON Orders.user_id = users.id INNER JOIN Pizzas ON Orders.pizzas_id = Pizzas.id";

        if (logger.isInfoEnabled()) {
            logger.info("Getting list of all orders from DB");
        }

        try (Connection cn = ConnectionConfig.getInstance().getConnection();  // соединение
             Statement st = cn.createStatement(); // стейтмент
             ResultSet rs = st.executeQuery(query)) { // результат запроса
            while (rs.next()) {
                allOrders.add(Order.newBuilder()
                        .orderId(rs.getInt(1))
                        .user(UserRepository.getInstance().getUserById(rs.getInt(2)))
                        .pizzaId(rs.getInt(3))
                        .pizzaName(rs.getString(4))
                        .orderQuantity(rs.getInt(5))
                        .status(rs.getString(6))
                        .build());
            }
        } catch (SQLException e) {
            logger.error("Error of getting AllOrdersFromUsers", e);
        }

        if (logger.isInfoEnabled()) {
            logger.info("Got allOrders for admin");
        }
        return allOrders;
    }

    // Возвращает список всех пицц заказанных конкретным пользователем
    public List<Order> getAllPizzasForCurrentUser(User user) {
        List<Order> PizzasForCurrentUser = new ArrayList<>();

        if (logger.isInfoEnabled()) {
            logger.info("Getting list of all orders for user " + user.getLogin() + " from DB");
        }

        try (Connection cn = ConnectionConfig.getInstance().getConnection();  // соединение
             PreparedStatement pt = cn.prepareStatement("SELECT pizzas.id, pizzas.name, orders.quantity, orders.status FROM Orders INNER JOIN Users on orders.user_id = ? AND users.id = ? INNER JOIN Pizzas ON orders.pizzas_id = pizzas.id")) { // стейтмент
            pt.setInt(1, user.getId());
            pt.setInt(2, user.getId());
            ResultSet rs = pt.executeQuery();  // результат запроса
            while (rs.next()) {
                PizzasForCurrentUser.add(
                        Order.newBuilder()
                                .user(user) // текущий user
                                .pizzaId(rs.getInt(1)) // id пиццы
                                .pizzaName(rs.getString(2)) // Название пиццы
                                .orderQuantity(rs.getInt(3)) // Количество
                                .status(rs.getString(4)) // Статус пиццы
                                .build());
            }
        } catch (SQLException e) {
            logger.error("Error of getting AllPizzasForCurrentUser", e);
        }

        if (logger.isInfoEnabled()) {
            logger.info("Got list of all orders for user " + user.getLogin() + " from DB");
        }
        return PizzasForCurrentUser;
    }

    // Меняет статус пиццы
    public void changeOrderStatus(Integer userId, Integer pizzaId, String status) {
        String querySet = "UPDATE orders SET orders.status = ? WHERE orders.user_id = ? AND orders.pizzas_id = ?";
        int i = 0;

        if (logger.isInfoEnabled()) {
            logger.info("Changing order status for pizza " + pizzaId + " in DB");
        }

        try (Connection cn = ConnectionConfig.getInstance().getConnection();
             PreparedStatement pt = cn.prepareStatement(querySet)) { // результат запроса
            pt.setString(1, status);
            pt.setInt(2, userId);
            pt.setInt(3, pizzaId);
            i = pt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Error of changing OrderStatus", e);
        }

        if (logger.isInfoEnabled()) {
            logger.info("Order status changed for pizza " + pizzaId + " with code " + i);
        }
    }

    // Возвращает количество конкретной пиццы заказанной конкретным пользователем или 0
    public int getQuantityThisPizza(Integer pizzaId, User user) {
        int quantity = 0;

        if (logger.isInfoEnabled()) {
            logger.info("Getting quantity for specific pizza for current user");
        }

        try (Connection cn = ConnectionConfig.getInstance().getConnection();
             Statement st = cn.createStatement(); // стейтмент
             ResultSet rs = st.executeQuery("SELECT orders.quantity FROM Orders WHERE orders.user_id = " + user.getId() + "  AND orders.pizzas_id = " + pizzaId)) { // результат запроса

            if (rs.next()) {
                System.out.println("Получаем текущее количество пиццы " + pizzaId + " в заказе у пользователя " + user.getId());
                quantity = rs.getInt(1);
            }

        } catch (SQLException e) {
            logger.error("Error of getting QuantityThisPizza", e);
        }

        if (logger.isInfoEnabled()) {
            logger.info("Quantity for specific pizza " + quantity + " for user " + user.getLogin());
        }
        return quantity;
    }

    // Создает соединение и вызывает методы добавления, изменения или удаления
    public void command(Integer pizzaId, Integer pizzaQuantity, User user, String command) {
        try (Connection cn = ConnectionConfig.getInstance().getConnection()) { // соединение
            switch (command) {
                case "addFirstPizza": {
                    if (logger.isInfoEnabled()) {
                        logger.info("Add first pizza with index " + pizzaId + "to DB");
                    }
                    addFirstPizzaToDB(pizzaId, user, cn);
                    break;
                }
                case "addPizza": {
                    if (logger.isInfoEnabled()) {
                        logger.info("Add pizza with index " + pizzaId + "to DB");
                    }
                    addPizzaToDB(pizzaId, pizzaQuantity, user, cn);
                    break;
                }
                case "removeOnePizza": {
                    if (logger.isInfoEnabled()) {
                        logger.info("Remove 1 pizza with index " + pizzaId + "from DB");
                    }
                    removeOnePizzaInDB(pizzaId, pizzaQuantity, user, cn);
                    break;
                }
                case "deletePizza": {
                    if (logger.isInfoEnabled()) {
                        logger.info("Remove all pizzas with index " + pizzaId + "from DB");
                    }
                    deletePizzaInDB(pizzaId, user, cn);
                    break;
                }
            }
        } catch (SQLException e) {
            logger.error("Error of using Command", e);
        }
    }

    // Создает пиццу конкретного пользователя в заказе
    private void addFirstPizzaToDB(Integer pizzaId, User user, Connection cn) {
        String query = "INSERT INTO orders (user_id, pizzas_id, quantity) VALUES (?,?,?)";

        try (PreparedStatement pt = cn.prepareStatement(query)) { // результат запроса
            pt.setInt(1, user.getId());
            pt.setInt(2, pizzaId);
            pt.setInt(3, 1);
            int i = pt.executeUpdate();

            if (logger.isInfoEnabled()) {
                logger.info("Pizza has added to DB with code " + i);
            }

        } catch (SQLException e) {
            logger.error("Error of adding FirstPizza", e);
        }
    }

    // Добавляет пиццу конкретного пользователя в заказ
    private void addPizzaToDB(Integer pizzaId, Integer pizzaQuantity, User user, Connection cn) {
        String query = "UPDATE orders SET orders.quantity = ? WHERE orders.user_id = ? AND orders.pizzas_id = ?";

        try (PreparedStatement pt = cn.prepareStatement(query)) { // результат запроса
            pt.setInt(1, pizzaQuantity);
            pt.setInt(2, user.getId());
            pt.setInt(3, pizzaId);
            int i = pt.executeUpdate();

            if (logger.isInfoEnabled()) {
                logger.info("Pizza has updated in DB with code " + i);
            }

            if (getOrderStatus(user.getId(), pizzaId).equals("done") || getOrderStatus(user.getId(), pizzaId).equals("in progress")) {
                changeOrderStatus(user.getId(), pizzaId, "getted"); // при добавлении пиццы меняем ее статус на начальный
            }

        } catch (SQLException e) {
            logger.error("Error of adding Pizza", e);
        }
    }

    // Уменьшает количество пиццы на 1 конкретного пользователя
    private void removeOnePizzaInDB(Integer pizzaId, Integer pizzaQuantity, User user, Connection cn) {
        String query = "UPDATE orders SET orders.quantity = ? WHERE orders.user_id = ? AND orders.pizzas_id = ?";

        try (PreparedStatement pt = cn.prepareStatement(query)) { // результат запроса
            pt.setInt(1, pizzaQuantity);
            pt.setInt(2, user.getId());
            pt.setInt(3, pizzaId);
            int i = pt.executeUpdate();

            if (logger.isInfoEnabled()) {
                logger.info("1 pizza has removed from DB with code " + i + ". New pizza quantity = " + pizzaQuantity);
            }

        } catch (SQLException e) {
            logger.error("Error of removing OnePizza", e);
        }
    }


    //Удаляет конкретную пиццу по id пользователя и пиццы
    private void deletePizzaInDB(Integer pizzaId, User user, Connection cn) {
        String query = "DELETE FROM orders WHERE orders.user_id=? AND orders.pizzas_id=?";
        try (PreparedStatement pt = cn.prepareStatement(query)) { // результат запроса
            pt.setInt(1, user.getId());
            pt.setInt(2, pizzaId);
            int i = pt.executeUpdate();

            if (logger.isInfoEnabled()) {
                logger.info("All quantity of pizza has removed from DB with code " + i);
            }

        } catch (SQLException e) {
            logger.error("Error of deleting Pizza", e);
        }
    }

    // Получает статус заказа
    private String getOrderStatus(Integer userId, Integer pizzaId) {
        String status = "getted";
        String queryGet = "SELECT orders.status FROM orders WHERE user_id = ? AND pizzas_id = ?";

        if (logger.isInfoEnabled()) {
            logger.info("Getting order status");
        }

        try (Connection cn = ConnectionConfig.getInstance().getConnection();
             PreparedStatement pt = cn.prepareStatement(queryGet)) { // результат запроса
            pt.setInt(1, userId);
            pt.setInt(2, pizzaId);
            ResultSet resultSet = pt.executeQuery();
            if (resultSet.next()) {
                status = resultSet.getString(1);
            }
        } catch (SQLException e) {
            logger.error("Error of getting OrderStatus", e);
        }

        if (logger.isInfoEnabled()) {
            logger.info("Order status is: " + status);
        }
        return status;
    }
}
