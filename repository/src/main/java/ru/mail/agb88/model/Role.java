package ru.mail.agb88.model;

/**
enum для ролей
 */
public enum Role {
    ADMINISTRATOR,
    USER
}
