package ru.mail.agb88.model;

/**
 Сущность заказа
 */
public class Order {
    private int orderId;
    private User user;
    private int pizzaId;
    private String pizzaName;
    private int orderQuantity;
    private String status;

    private Order(Builder builder) {
        setOrderId(builder.orderId);
        setUser(builder.user);
        setPizzaId(builder.pizzaId);
        setPizzaName(builder.pizzaName);
        setOrderQuantity(builder.orderQuantity);
        setStatus(builder.status);
    }

    public static Builder newBuilder (){
        return new Builder();
    }

    public int getOrderId() {
        return orderId;
    }

    private void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public User getUser() {
        return user;
    }

    private void setUser(User user) {
        this.user = user;
    }

    public int getPizzaId() {
        return pizzaId;
    }

    private void setPizzaId(int pizzaId) {
        this.pizzaId = pizzaId;
    }

    public String getPizzaName() {
        return pizzaName;
    }

    private void setPizzaName(String pizzaName) {
        this.pizzaName = pizzaName;
    }

    public int getOrderQuantity() {
        return orderQuantity;
    }

    private void setOrderQuantity(int orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public String getStatus() {
        return status;
    }

    private void setStatus(String status) {
        this.status = status;
    }

    public static final class Builder {
        private int orderId;
        private User user;
        private int pizzaId;
        private String pizzaName;
        private int orderQuantity;
        private String status;

        public Builder orderId(int orderId) {
            this.orderId = orderId;
            return this;
        }

        public Builder user(User user) {
            this.user = user;
            return this;
        }

        public Builder pizzaId(int pizzaId) {
            this.pizzaId = pizzaId;
            return this;
        }

        public Builder pizzaName(String pizzaName) {
            this.pizzaName = pizzaName;
            return this;
        }

        public Builder orderQuantity(int orderQuantity) {
            this.orderQuantity = orderQuantity;
            return this;
        }

        public Builder status(String status) {
            this.status = status;
            return this;
        }

        public Order build(){
            return new Order(this);
        }
    }
}
