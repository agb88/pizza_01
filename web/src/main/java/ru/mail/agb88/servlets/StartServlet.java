package ru.mail.agb88.servlets;

import ru.mail.agb88.model.Pizza;
import ru.mail.agb88.service.PizzaService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**

 */
public class StartServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Pizza> pizzas = PizzaService.getInstance().getAllPizzas();
        req.setAttribute("pizzas", pizzas);
        req.getRequestDispatcher(req.getContextPath() + "/WEB-INF/JSP/index.jspx").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
