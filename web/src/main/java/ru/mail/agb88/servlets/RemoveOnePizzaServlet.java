package ru.mail.agb88.servlets;

import org.apache.log4j.Logger;
import ru.mail.agb88.model.Order;
import ru.mail.agb88.model.User;
import ru.mail.agb88.service.PizzaService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
Сервлет для уменьшщения на 1 количества пиццы у конкретного пользователя из БД. /change
 */
public class RemoveOnePizzaServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(RemoveOnePizzaServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();
        Integer pizzaId = null;

        try {
            pizzaId = Integer.parseInt(req.getParameter("pizzaId")); // Парсим полученные параметры изменения заказа
        } catch (NumberFormatException e) {
            logger.error("pizzaId not found", e);
        }

        User user = (User) session.getAttribute("user"); // получаем пользователя, который сделал заказ

        if (user == null) {
            if (logger.isInfoEnabled()) {
                logger.info("Login failed.");
            }
            resp.sendRedirect(req.getContextPath() + "/");
            return;
        }

        PizzaService.getInstance().removeOnePizza(pizzaId, user); // удаляем 1 пиццу

        List<Order> orders = PizzaService.getInstance().getAllPizzasForCurrentUser(user); // список заказов данного пользователя

        if (logger.isInfoEnabled()) {
            logger.info("Got orders for user " + user.getLogin() + " , transfer to session");
        }

        session.setAttribute("orders",orders);

        req.getRequestDispatcher(req.getContextPath() + "/WEB-INF/JSP/order.jspx").forward(req,resp);


    }
}
