package ru.mail.agb88.servlets;

import org.apache.log4j.Logger;
import ru.mail.agb88.model.User;
import ru.mail.agb88.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Фильтр, который проверяет залогинены или нет.
 * Сначала пытаемся достать логин и пароль из сессии. Если достали, то сверяем и присваиваем user в сессию.
 * Если не достали, то продолжаем дальше
 */
public class LoginAndPasswordServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(LoginAndPasswordServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession(); // Получаем сессию

        String login = req.getParameter("login");        // Парсим полученные параметры логина и пароля
        String password = req.getParameter("password");

        if (UserService.getInstance().checkLoginAndPassword(login, password)) { // Перенапрявляем на ввод логина по новой, если нет логина
            User user = UserService.getInstance().getUserByLoginAndPassword(login, password);//(login, password); // Получаем пользователя из БД по логину и паролю. Если нет, то null

            if (user != null) { // если мы не залогинены и пароль с логином совпали с БД, то логинемся
                session.setAttribute("user", user);

                if (logger.isInfoEnabled()) {
                    logger.info("Login is valid");
                }

                req.getRequestDispatcher(req.getContextPath() + "/").forward(req, resp);
                return;
            }
        }

        if (logger.isInfoEnabled()) {
            logger.info("Login is not valid");
        }

        resp.sendRedirect(req.getContextPath() + "/");
    }
}
