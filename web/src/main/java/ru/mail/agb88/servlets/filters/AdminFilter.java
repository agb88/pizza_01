package ru.mail.agb88.servlets.filters;

import org.apache.log4j.Logger;
import ru.mail.agb88.model.User;
import ru.mail.agb88.service.UserService;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Фильтр стоит на админ странице /admin и проверяет, чтобы туда попал только админ
 */
public class AdminFilter implements Filter {
    private static final Logger logger = Logger.getLogger(AdminFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession(); // Получаем сессию
        User thisUser = (User) session.getAttribute("user"); // Проверяем, если был залогинен раньше

        if (thisUser == null || !UserService.getInstance().checkAdmin(thisUser)) { // если не вошли или вошел не админ
            if (logger.isInfoEnabled()) {
                logger.info("Try to enter to admin panel not by admin");
            }
            response.sendRedirect(request.getContextPath() + "/");
        } else {
            if (logger.isInfoEnabled()) {
                logger.info("Enter by admin");
            }
            filterChain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }
}
