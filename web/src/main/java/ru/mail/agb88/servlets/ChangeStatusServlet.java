package ru.mail.agb88.servlets;

import ru.mail.agb88.service.PizzaService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Сервлет для смены статуса. /changestatus
 */
public class ChangeStatusServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();

        Integer userId = Integer.parseInt(req.getParameter("userId")); // Парсим полученные параметры изменения заказа
        Integer pizzaId = Integer.parseInt(req.getParameter("pizzaId")); // Парсим полученные параметры изменения заказа
        String status = req.getParameter("status");

        // Меняем статус
        PizzaService.getInstance().changeOrderStatus(userId, pizzaId, status);
        req.getRequestDispatcher(req.getContextPath() + "/admin").forward(req, resp);
    }
}
