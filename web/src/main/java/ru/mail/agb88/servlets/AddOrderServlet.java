package ru.mail.agb88.servlets;


import org.apache.log4j.Logger;
import ru.mail.agb88.model.Order;
import ru.mail.agb88.model.User;
import ru.mail.agb88.service.PizzaService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Этот сервлет
 * 1. отправляет в БД заказ от пользователя, вытягивает все заказы данного пользователя с БД и отправляет на JSPX c кнопками:
 * а) добавить еще заказ (возвращает на index.jspx)
 * б) изменить заказ (кнопка отправляет на сервлет, который по id определяет в какой строке нужно изменить
 * количество и меняет его, а затем возвращает на эту же страницу)
 * в) удалить заказ(отправляет на сервлет, который удаляет пиццу из заказа и возвращает на эту же страницу с
 * оставшимися пиццами в заказе).
 */
public class AddOrderServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(AddOrderServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();
        Integer pizzaId = null;

        User user = (User) session.getAttribute("user"); // получаем пользователя, который сделал заказ

        if (user == null) {
            if (logger.isInfoEnabled()) {
                logger.info("Login failed");
            }
            resp.sendRedirect(req.getContextPath() + "/");
            return;
        }

        try { // Парсим полученные параметры заказа
            pizzaId = Integer.parseInt(req.getParameter("pizzaId"));
        } catch (NumberFormatException e) {
            logger.error("pizzaId not found", e);
        }

        if (pizzaId != null) { // если id число, которые получено из параметра, то это номер пиццы в заказе
            PizzaService.getInstance().addPizza(pizzaId, user);
        }

        List<Order> orders = PizzaService.getInstance().getAllPizzasForCurrentUser(user); // список заказов данного пользователя

        if (logger.isInfoEnabled()) {
            logger.info("Got orders for user " + user.getLogin() + ", transfer to session");
        }

        session.setAttribute("orders", orders);

        req.getRequestDispatcher(req.getContextPath() + "/WEB-INF/JSP/order.jspx").forward(req, resp);
    }
}
