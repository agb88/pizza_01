package ru.mail.agb88.servlets.listeners;

import org.apache.log4j.Logger;
import ru.mail.agb88.repository.DataBaseRepository;

import javax.servlet.ServletContextEvent;

/**
 Листенер, который инициализирует таблицы в БД
 */
public class ServletContextListener implements javax.servlet.ServletContextListener{

    private static final Logger logger = Logger.getLogger(ServletContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        DataBaseRepository.getInstance().initializeDB();
        if (logger.isInfoEnabled()) {
            logger.info("Data Base initialized");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
