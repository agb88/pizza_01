package ru.mail.agb88.servlets;

import org.apache.log4j.Logger;
import ru.mail.agb88.model.Order;
import ru.mail.agb88.service.PizzaService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 Сервлет для отображения в админке всех заказов. /admin
 */
public class AdminServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(AdminServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();

        // Получаем список всех заказов
        List<Order> allOrders = PizzaService.getInstance().getAllOrdersFromUsers();

        if (logger.isInfoEnabled()) {
            logger.info("Transfer allOrders to session");
        }

        session.setAttribute("allOrders",allOrders);
        req.getRequestDispatcher(req.getContextPath() + "/WEB-INF/JSP/admin.jspx").forward(req,resp);
    }

}
