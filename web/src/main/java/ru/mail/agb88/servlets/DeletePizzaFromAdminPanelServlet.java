package ru.mail.agb88.servlets;

import org.apache.log4j.Logger;
import ru.mail.agb88.model.User;
import ru.mail.agb88.service.PizzaService;
import ru.mail.agb88.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Удаление пиццы из меню админа
 */
public class DeletePizzaFromAdminPanelServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(DeletePizzaFromAdminPanelServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Integer pizzaId = Integer.parseInt(req.getParameter("pizzaId")); // Парсим полученные параметры изменения заказа
        Integer userId = Integer.parseInt(req.getParameter("userId")); // Парсим полученные параметры изменения заказа

        User user = UserService.getInstance().getUserById(userId); // получаем пользователя из БД по id

        if (user == null) {
            if (logger.isInfoEnabled()) {
                logger.info("There is no such user");
            }
            resp.sendRedirect(req.getContextPath() + "/");
            return;
        }

        PizzaService.getInstance().deletePizza(pizzaId, user); // удаляем пиццу у пользователя user

        req.getRequestDispatcher(req.getContextPath() + "/admin").forward(req, resp);
    }
}
