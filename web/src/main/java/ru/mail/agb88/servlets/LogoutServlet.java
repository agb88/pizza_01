package ru.mail.agb88.servlets;

import org.apache.log4j.Logger;
import ru.mail.agb88.model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Фильтр выхода из логина
 */
public class LogoutServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(LogoutServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession(); // Получаем сессию

        User thisUser = (User) session.getAttribute("user"); // Проверяем, если был залогинен раньше
        String exit = req.getParameter("exit");

        if (thisUser != null && exit != null) {
            if (logger.isInfoEnabled()) {
                logger.info("Logout");
            }

            session.removeAttribute("user");
            session.removeAttribute("orders");
            session.removeAttribute("OrderForAdmin");

            resp.sendRedirect(req.getContextPath() + "/");
        }
    }
}
